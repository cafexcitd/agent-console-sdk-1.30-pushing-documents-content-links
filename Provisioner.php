<?php

// class is responsible for provisioning
class Provisioner {

    private $gatewayIp;
    private $secure;

    ///////////////////////////////////////////
    //
    // Publicly accessible methods

    // constructor
    public function __construct($gatewayIp = '192.168.0.99', $secure = false) {
        $this->gatewayIp = $gatewayIp;
        $this->secure = $secure;
    }

    // creates JSON suitable for the agent described by the method params
    // returns the JSON response from the Web Gateway
    public function provisionAgent($username, $domain, $topic) {
        $json = $this->buildAgentJson($username, $domain, $topic);
        return $this->provision($json);
    }


    ///////////////////////////////////////////
    //
    // Internal method to fulfill the above API
    //
    ///////////////////////////////////////////



    // builds provisioning JSON suitable for an agent
    private function buildAgentJson($username, $domain, $topic) {
        $json = '{
            "webAppId": "FUSIONWEBGATEWAY-D3SJN5E5SIZU",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "secure": %s,
                "host": "%s",
                "port": "%s"
            },
            "voice": {
                "username": "%s",
                "domain": "%s"
            },
            "additionalAttributes": {
                "AED2.allowedTopic": "%s",
                "AED2.metadata": {
                    "role": "agent",
                    "name": "Name displayed"
                }
            }
        }';

        $secureString = $this->secure ? 'true' : 'false';
        $portString = $this->secure ? '8443' : '8080';

        return sprintf($json, $secureString, $this->gatewayIp, $portString, $username, $domain, $topic);
    }

    // sends the given provisioning JSON to the Web Gateway
    // returns the JSON received from the Web Gateway
    private function provision($json) {

        // configure the curl options
        $ch = curl_init($this->buildGatewayUri());

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json)
        ]);

        // execute HTTP POST & close the connection
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    // returns a URI corresponding to the address of the web gateway
    private function buildGatewayUri () {
        return $this->secure
            ? 'https://' . $this->gatewayIp . ':8443/gateway/sessions/session'
            : 'http://' . $this->gatewayIp . ':8080/gateway/sessions/session';
    }
}

?>
